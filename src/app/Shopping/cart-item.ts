import { Pizza } from "../Pizza-menu/pizza";

export class CartItem {
    pizza: Pizza;
    quantity: number;

    constructor(pizza: Pizza, quantity: number){
        this.pizza = pizza;
        this.quantity = quantity;
    }

    incrementQuantity(): void{
        this.quantity += 1;
    }

    decrementQuantity(): void{
        this.quantity -= 1;
    }

    calcPrice(): number{
        return this.pizza.price * this.quantity;
    }
    
}
