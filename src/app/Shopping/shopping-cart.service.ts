import { Injectable } from '@angular/core';
import { CartItem } from './cart-item';
import { Pizza } from '../Pizza-menu/pizza';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  cartItems: CartItem[] = [];

  constructor() { }

  getCartItems(): CartItem[]{
    return this.cartItems;
  }

  addItem(newItem: CartItem): void {
    for(const item of this.cartItems){
      if(item.pizza.name === newItem.pizza.name){
        item.quantity +=1;
        return;
      }
    }
    this.cartItems.push(newItem);
  }

  addPizza(pizza: Pizza): void {
    this.addItem(new CartItem(pizza, 1));
  }

  calcTotalPrice(): number {
    let totalPrice = 0;
    this.cartItems.forEach(i => totalPrice += i.calcPrice());
    return totalPrice;

  }

  
}
