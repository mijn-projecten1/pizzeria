export interface Pizza {
    imgUrl: string;
    name: string;
    discreption: string;
    price: number;
}
