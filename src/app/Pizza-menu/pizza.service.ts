import { Injectable } from '@angular/core';
import { Pizza } from './pizza';

@Injectable({
  providedIn: 'root'
})
export class PizzaService {

  private pizzaMenu: Array<Pizza> =[
    {
      imgUrl: "https://www.dominos.be/ManagedAssets/BE/product/PMAR/BE_PMAR_all_hero_9585.jpg?v1617539712",
      name: 'Margherita',
      discreption: 'Tomato sauce, mozzarella, extra mozzarella & pizza herbs',
      price: 9.99
    },
    {
      imgUrl: "https://www.dominos.be/ManagedAssets/BE/product/PAM2/BE_PAM2_all_hero_9585.jpg?v1020543461",
      name: 'Pepperoni',
      discreption: 'Tomato, mozzarella & pepperoni sauce',
      price: 11.99
    },
    {
      imgUrl: "https://www.dominos.be/ManagedAssets/BE/product/PHAW/BE_PHAW_all_hero_9585.jpg?v1643905217",
      name: 'Hawaiian',
      discreption: 'Tomato sauce, mozzarella, ham, pineapple & extra mozzarella',
      price: 12.99
    },
    {
      imgUrl: "https://www.dominos.be/ManagedAssets/BE/product/PCAN/BE_PCAN_all_hero_9585.jpg?v-1943604808",
      name: "Meat Lover's",
      discreption: 'BBQ sauce, mozzarella, beef, merguez, chicken & a swirl of BBQ sauce',
      price: 14.99
    },
    {
      imgUrl: "https://www.dominos.be/ManagedAssets/BE/product/PVSP/BE_PVSP_all_hero_9585.jpg?v1318604103",
      name: 'Vegetarian',
      discreption: 'Tomato sauce, vegan cheese, fresh spinach, fresh tomatoes, bell peppers, jalapeños & onions',
      price: 12.99
    },
    {
      imgUrl: "https://www.dominos.be/ManagedAssets/BE/product/PBBC/BE_PBBC_all_hero_9585.jpg?v1251780617",
      name: 'BBQ Chiken',
      discreption: 'Tomato sauce, mozzarella, chicken, onions, peppers & swirl BBQ sauce', 
      price: 13.99
    },
  ];

  gatAllPizza(): Pizza[]{
    return this.pizzaMenu;
  }

  constructor() { }
}
