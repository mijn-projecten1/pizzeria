import { Component } from '@angular/core';
import { CartItem } from '../Shopping/cart-item';
import { ShoppingCartService } from '../Shopping/shopping-cart.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-shopping-cart',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './shopping-cart.component.html',
  styleUrl: './shopping-cart.component.css'
})
export class ShoppingCartComponent {
  cartItems: CartItem[] = [];
  constructor(private shoppingCartservice: ShoppingCartService){
   this.cartItems = this.shoppingCartservice.getCartItems();
  }

  getTotalPrice(): number {
    return this.shoppingCartservice.calcTotalPrice();
  }

}
