import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MenuComponent } from './menu/menu.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';

export const routes: Routes = [
    {path:'', component:HomeComponent},
    {path: 'menu', component:MenuComponent},
    {path: 'cart', component: ShoppingCartComponent},
];
