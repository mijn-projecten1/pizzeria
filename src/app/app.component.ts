import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NavbarComponent } from "./navbar/navbar.component";
import { FooterComponent } from "./footer/footer.component";
import { HomeComponent } from "./home/home.component";
import { MenuComponent } from './menu/menu.component';
import { ShoppingCartComponent } from "./shopping-cart/shopping-cart.component";
@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrl: './app.component.css',
    imports: [RouterOutlet, NavbarComponent, FooterComponent, HomeComponent, MenuComponent, ShoppingCartComponent]
})
export class AppComponent {
  title = 'ng-pizzeria';
}
