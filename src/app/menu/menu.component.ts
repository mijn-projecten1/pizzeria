import { Component } from '@angular/core';
import { Pizza } from '../Pizza-menu/pizza';
import { PizzaService } from '../Pizza-menu/pizza.service';
import { CommonModule } from '@angular/common';
import { ShoppingCartService } from '../Shopping/shopping-cart.service';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-menu',
  standalone: true,
  imports: [CommonModule, RouterLink],
  templateUrl: './menu.component.html',
  styleUrl: './menu.component.css'
})
export class MenuComponent {
  pizzaMenu: Pizza[] = [];

  constructor(private pizzaservice: PizzaService, private shoppingCartService: ShoppingCartService ){
    this.pizzaMenu = this.pizzaservice.gatAllPizza();
  }

  addPizzaToShoppigCart(pizza: Pizza): void {
    this.shoppingCartService.addPizza(pizza);
  }

}
